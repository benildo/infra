namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModMonitor")]
    public partial class ModMonitor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModMonitorID { get; set; }

        public int? FabricanteID { get; set; }

        public int? FornecedorID { get; set; }

        [StringLength(255)]
        public string Modelo { get; set; }
    }
}
