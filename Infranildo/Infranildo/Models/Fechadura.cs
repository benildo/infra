namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Fechadura")]
    public partial class Fechadura
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FechaduraID { get; set; }

        public int? FuncionarioID { get; set; }

        public int? NumeroCadastrado { get; set; }
    }
}
