namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ocorrencia")]
    public partial class Ocorrencia
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OcorrenciaID { get; set; }

        public int? PlaquetaID { get; set; }

        public string Descrição { get; set; }

        public int? FuncionarioID { get; set; }

        public DateTime? Data { get; set; }
    }
}
