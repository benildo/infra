namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Fornecedor")]
    public partial class Fornecedor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FornecedorID { get; set; }

        [StringLength(255)]
        public string Nome { get; set; }
    }
}
