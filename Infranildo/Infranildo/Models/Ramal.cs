namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ramal")]
    public partial class Ramal
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RamalID { get; set; }

        public int? FuncionarioID { get; set; }

        public int? ModeloID { get; set; }

        [StringLength(255)]
        public string Ip { get; set; }

        [Column("Ramal")]
        public int? Ramal1 { get; set; }

        [StringLength(255)]
        public string Serial { get; set; }
    }
}
