namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModCftv")]
    public partial class ModCftv
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModCftvID { get; set; }

        [StringLength(255)]
        public string Nome { get; set; }

        public int? FabricanteID { get; set; }

        [StringLength(255)]
        public string Modelo { get; set; }
    }
}
