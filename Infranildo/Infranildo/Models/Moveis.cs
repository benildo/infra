namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Moveis
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdMovel { get; set; }

        public int? IdFuncionario { get; set; }

        public int? Modelo { get; set; }

        public int? IdPlaqueta { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool CadastradoTotvs { get; set; }
    }
}
