namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Nobreak")]
    public partial class Nobreak
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NobreakID { get; set; }

        public int? Modelo { get; set; }

        public int? FabricanteID { get; set; }

        public int? PlaquetaID { get; set; }
    }
}
