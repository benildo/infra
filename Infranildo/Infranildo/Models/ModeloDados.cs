namespace Infranildo.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModeloDados : DbContext
    {
        public ModeloDados()
            : base("name=ModeloDados")
        {
        }

        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Celular> Celular { get; set; }
        public virtual DbSet<Cftv> Cftv { get; set; }
        public virtual DbSet<Email> Email { get; set; }
        public virtual DbSet<Fabricante> Fabricante { get; set; }
        public virtual DbSet<Fechadura> Fechadura { get; set; }
        public virtual DbSet<Fornecedor> Fornecedor { get; set; }
        public virtual DbSet<Funcionario> Funcionario { get; set; }
        public virtual DbSet<Impressora> Impressora { get; set; }
        public virtual DbSet<ModCftv> ModCftv { get; set; }
        public virtual DbSet<ModImpressora> ModImpressora { get; set; }
        public virtual DbSet<ModMonitor> ModMonitor { get; set; }
        public virtual DbSet<ModMovel> ModMovel { get; set; }
        public virtual DbSet<ModNobreak> ModNobreak { get; set; }
        public virtual DbSet<ModPc> ModPc { get; set; }
        public virtual DbSet<ModSoftware> ModSoftware { get; set; }
        public virtual DbSet<ModSwitch> ModSwitch { get; set; }
        public virtual DbSet<ModTeclado> ModTeclado { get; set; }
        public virtual DbSet<ModTelefone> ModTelefone { get; set; }
        public virtual DbSet<Monitor> Monitor { get; set; }
        public virtual DbSet<Moveis> Moveis { get; set; }
        public virtual DbSet<Nobreak> Nobreak { get; set; }
        public virtual DbSet<Ocorrencia> Ocorrencia { get; set; }
        public virtual DbSet<Office> Office { get; set; }
        public virtual DbSet<Pc> Pc { get; set; }
        public virtual DbSet<Plaqueta> Plaqueta { get; set; }
        public virtual DbSet<Ramal> Ramal { get; set; }
        public virtual DbSet<Setor> Setor { get; set; }
        public virtual DbSet<Switch> Switch { get; set; }
        public virtual DbSet<Teclado> Teclado { get; set; }
        public virtual DbSet<Windows> Windows { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
