namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Setor")]
    public partial class Setor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SetorID { get; set; }

        [StringLength(255)]
        public string Nome { get; set; }
    }
}
