namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Impressora")]
    public partial class Impressora
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ImpressoraID { get; set; }

        public int? FuncionarioID { get; set; }

        public int? ModImpressoraID { get; set; }

        public int? PlaquetaID { get; set; }

        [Key]
        [Column("No Totvs", Order = 1)]
        public bool No_Totvs { get; set; }
    }
}
