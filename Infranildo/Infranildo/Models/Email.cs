namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Email")]
    public partial class Email
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmailID { get; set; }

        public int? FuncionarioID { get; set; }

        [StringLength(255)]
        public string Conta { get; set; }

        [StringLength(255)]
        public string Senha { get; set; }

        [StringLength(255)]
        public string Finalidade { get; set; }
    }
}
