namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModNobreak")]
    public partial class ModNobreak
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModNobreakID { get; set; }

        [StringLength(255)]
        public string Modelo { get; set; }

        public int? FabricanteID { get; set; }
    }
}
