namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModMovel")]
    public partial class ModMovel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModMovelID { get; set; }

        [StringLength(255)]
        public string Modelo { get; set; }

        public int? FabricanteID { get; set; }

        [StringLength(255)]
        public string Referencia { get; set; }

        public int? FornecedorID { get; set; }
    }
}
