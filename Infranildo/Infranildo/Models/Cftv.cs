namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cftv")]
    public partial class Cftv
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CftvID { get; set; }

        public int? ModCftvID { get; set; }

        public int? PlaquetaID { get; set; }

        [StringLength(255)]
        public string Local { get; set; }
    }
}
