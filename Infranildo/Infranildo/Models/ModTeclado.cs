namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModTeclado")]
    public partial class ModTeclado
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModTecladoID { get; set; }

        public int? FornecedorID { get; set; }

        public int? FabricanteID { get; set; }

        [StringLength(255)]
        public string Modelo { get; set; }
    }
}
