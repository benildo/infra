namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Teclado")]
    public partial class Teclado
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TecladoID { get; set; }

        public int? ResponsavelID { get; set; }

        public int? FabricanteID { get; set; }

        public int? ModTecladoID { get; set; }

        public int? PlaquetaID { get; set; }
    }
}
