namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Pc")]
    public partial class Pc
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PcID { get; set; }

        public int? FuncionarioID { get; set; }

        public int? ModPcID { get; set; }

        [StringLength(255)]
        public string Processador { get; set; }

        [StringLength(255)]
        public string Memória { get; set; }

        [StringLength(255)]
        public string MacEth { get; set; }

        [StringLength(255)]
        public string MacWifi { get; set; }

        [StringLength(255)]
        public string Teamviewer { get; set; }

        [StringLength(255)]
        public string NomePc { get; set; }

        [StringLength(255)]
        public string Série { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlaquetaID { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool Termo { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool Auditorado { get; set; }
    }
}
