namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Monitor")]
    public partial class Monitor
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MonitorID { get; set; }

        public int? FuncionarioID { get; set; }

        public int? FabricanteID { get; set; }

        public int? Modelo { get; set; }

        public int? PlaquetaID { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool CadastradoTotvs { get; set; }
    }
}
