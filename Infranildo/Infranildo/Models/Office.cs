namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Office")]
    public partial class Office
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OfficeID { get; set; }

        public int? ModSoftwareID { get; set; }

        [StringLength(255)]
        public string Chave { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool Ativado { get; set; }

        public int? FuncionarioID { get; set; }
    }
}
