namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Celular")]
    public partial class Celular
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CelularID { get; set; }

        public int? ModTelefoneID { get; set; }

        public int? FuncionarioID { get; set; }

        [StringLength(255)]
        public string Imei { get; set; }

        [StringLength(255)]
        public string Serial { get; set; }

        [StringLength(255)]
        public string Numero { get; set; }
    }
}
