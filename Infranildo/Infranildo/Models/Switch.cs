namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Switch")]
    public partial class Switch
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SwitchID { get; set; }

        public int? FuncionarioID { get; set; }

        public int? SetorID { get; set; }

        public int? FabricanteID { get; set; }

        public int? PlaquetaID { get; set; }
    }
}
