namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModSoftware")]
    public partial class ModSoftware
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ModSoftwareID { get; set; }

        [StringLength(255)]
        public string Modelo { get; set; }

        public int? FabricanteID { get; set; }
    }
}
