namespace Infranildo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Funcionario")]
    public partial class Funcionario
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FuncionarioID { get; set; }

        [StringLength(255)]
        public string Nome { get; set; }

        public int? SetorID { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        public DateTime? DataAdmissao { get; set; }

        [StringLength(255)]
        public string Cargo { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool Termo { get; set; }
    }
}
