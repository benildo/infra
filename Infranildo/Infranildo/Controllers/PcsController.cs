﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Infranildo.Models;

namespace Infranildo.Controllers
{
    public class PcsController : Controller
    {
        private ModeloDados db = new ModeloDados();

        // GET: Pcs
        public ActionResult Index()
        {
            return View(db.Pc.ToList());
        }

        // GET: Pcs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pc pc = db.Pc.Find(id);
            if (pc == null)
            {
                return HttpNotFound();
            }
            return View(pc);
        }

        // GET: Pcs/Create
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PcID,PlaquetaID,Termo,Auditorado,FuncionarioID,ModPcID,Processador,Memória,MacEth,MacWifi,Teamviewer,NomePc,Série")] Pc pc)
        {
            if (ModelState.IsValid)
            {
                db.Pc.Add(pc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pc);
        }

        // GET: Pcs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pc pc = db.Pc.Find(id);
            if (pc == null)
            {
                return HttpNotFound();
            }
            return View(pc);
        }

        // POST: Pcs/Edit/5      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PcID,PlaquetaID,Termo,Auditorado,FuncionarioID,ModPcID,Processador,Memória,MacEth,MacWifi,Teamviewer,NomePc,Série")] Pc pc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pc);
        }

        // GET: Pcs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pc pc = db.Pc.Find(id);
            if (pc == null)
            {
                return HttpNotFound();
            }
            return View(pc);
        }

        // POST: Pcs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pc pc = db.Pc.Find(id);
            db.Pc.Remove(pc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
