﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Infranildo.Models;

namespace Infranildo.Controllers
{
    public class CelularesController : Controller
    {
        private ModeloDados db = new ModeloDados();

        
        public ActionResult Index()
        {
            return View(db.Celular.ToList());
        }

        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Celular celular = db.Celular.Find(id);
            if (celular == null)
            {
                return HttpNotFound();
            }
            return View(celular);
        }

        
        public ActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CelularID,ModTelefoneID,FuncionarioID,Imei,Serial,Numero")] Celular celular)
        {
            if (ModelState.IsValid)
            {
                db.Celular.Add(celular);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(celular);
        }

        // GET: Celulares/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Celular celular = db.Celular.Find(id);
            if (celular == null)
            {
                return HttpNotFound();
            }
            return View(celular);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CelularID,ModTelefoneID,FuncionarioID,Imei,Serial,Numero")] Celular celular)
        {
            if (ModelState.IsValid)
            {
                db.Entry(celular).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(celular);
        }

        
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Celular celular = db.Celular.Find(id);
            if (celular == null)
            {
                return HttpNotFound();
            }
            return View(celular);
        }

        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Celular celular = db.Celular.Find(id);
            db.Celular.Remove(celular);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
